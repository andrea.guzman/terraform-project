# Configure the AWS Provider
provider "aws" {
  region  = "us-west-2"
  access_key = var.aws_key
  secret_key = var.aws_secret
}

resource "aws_s3_bucket" "bucket-monse" {
    bucket = "mybucket-monse-tf"
    acl    = "private"
    tags = {
        Name = "mybucket-tf"
    }
}
